# Energy Monitoring - Shelly Plug S

[[_TOC_]]

## Description

Energy monitoring by pulling power usage from [Shelly Plug S](https://shelly.cloud/products/shelly-plug-s-smart-home-automation-device/) and forwarding it to an InfluxDB bucket.
